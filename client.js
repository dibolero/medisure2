let { connect } = require('lotion')
let GCI = 'b987352528d43bc9591e120ab03e9dc057c39cff4c5ae71b5ba626adefe6403b'
async function main(){
	let { state, send } = await connect(GCI)

let count = await state.count
console.log(count) // 0

let result = await send({ nonce: 0 })
console.log(result)

count = await state.count
console.log(count) // 1
}
main()