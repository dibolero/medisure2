let lotion = require('lotion')

let app = lotion({
	"initialState": {
		"count": 0,
		"drug":
		  [
		    
		    
		  ]
		

	},
	rpcPort:25555
})

function transactionHandler(state, transaction) {
console.log("WAW");
var DrugModel = {
		      "health":"",
		      "hash":"",
		      "timeStamp":"",
		      "manufacturer":
		      {
		        "manufacturerHash":"none",
		        "timeStamp":"",
		        "status":""
		      },
		      "shipping":{
		        
		        "shippingHash":"none",
		        "timeStamp":"",
		        "status":""
		      },
		      "warehouse":{
		        "warehouseHash":"none",
		        "timeStamp":"",
		        "status":""
		      },
		      "pharmacy":{
		        "pharmacyHash":"none",
		        "timeStamp":"",
		        "status":"",
		        "pharmacySellerHash":"none",
		        "customer":""
		      }
		    }
		    console.log(JSON.stringify(transaction))
		  
		    	if(transaction.transactionType === "manufacturerCreate"){

			    	DrugModel.manufacturer.manufacturerHash = transaction.userHash;
			    	DrugModel.manufacturer.timeStamp = transaction.timeStamp;
			    	DrugModel.manufacturer.status = transaction.status;
			    	DrugModel.health = "genuine";
			    	DrugModel.hash = transaction.hash;	    	
			    	state.drug.push(DrugModel); 
			    	state.count++;

		    
			    }else{

				    if(state.drug.find(x=> x.hash == transaction.hash) != null){
					    if(transaction.transactionType === "shippingVerify"){
					    	if(state.drug.find(x=> x.hash === transaction.hash).manufacturer.manufacturerHash != "none" && state.drug.find(x=> x.hash === transaction.hash).health !="compromised" ){
					    		
					    		state.drug.find(x=> x.hash === transaction.hash).shipping.shippingHash = transaction.userHash;			    
						    	state.drug.find(x=> x.hash === transaction.hash).shipping.timeStamp = transaction.timeStamp;
						    	state.drug.find(x=> x.hash === transaction.hash).shipping.status = transaction.status;
						    	state.drug.find(x=> x.hash === transaction.hash).shipping.health = "genuine"; 
						    	state.count++;	
					    	}else{					    		
					    		state.drug.find(x=> x.hash === transaction.hash).health = "compromised";
					    		state.count++;
					    		throw "This drug may have been compromised as it has skipped verification. Please contact the manufacturer now."
					    	}
					    			    	
					    }

					    if(transaction.transactionType === "warehouseVerify"){
					    	if(state.drug.find(x=> x.hash === transaction.hash).manufacturer.manufacturerHash != "none" && state.drug.find(x=> x.hash === transaction.hash).shipping.shippingHash != "none" && state.drug.find(x=> x.hash === transaction.hash).health !="compromised"){						    							    	
						    	state.drug.find(x=> x.hash === transaction.hash).warehouse.warehouseHash = transaction.userHash;		    
						    	state.drug.find(x=> x.hash === transaction.hash).warehouse.timeStamp = transaction.timeStamp;
						    	state.drug.find(x=> x.hash === transaction.hash).warehouse.status = transaction.status;
						    	state.drug.find(x=> x.hash === transaction.hash).warehouse.health = "genuine"; 			   
						    	state.count++; 	
					    	}else{
					    		if(state.drug.find(x=> x.hash === transaction.hash).health == "compromised"){
					    			state.drug.find(x=> x.hash === transaction.hash).health = "compromised";
					    			state.count++;
					    			throw "This drug has been marked compromised as it has skipped verification. Please contact the manufacturer now."
					    		}else{
					    			state.drug.find(x=> x.hash === transaction.hash).health = "compromised";
					    			state.count++;
					    			throw "This drug may have been compromised as it has skipped verification. Please contact the manufacturer now."

					    		}
					    		
					    	}
					    	
					    }


					    if(transaction.transactionType === "pharmacyVerify"){
							if(state.drug.find(x=> x.hash === transaction.hash).manufacturer.manufacturerHash != "none" && state.drug.find(x=> x.hash === transaction.hash).shipping.shippingHash != "none" && state.drug.find(x=> x.hash === transaction.hash).warehouse.warehouseHash != "none" && state.drug.find(x=> x.hash === transaction.hash).health !="compromised"){
								


								state.drug.find(x=> x.hash === transaction.hash).pharmacy.pharmacyHash = transaction.userHash;		    
						    	state.drug.find(x=> x.hash === transaction.hash).pharmacy.timeStamp = transaction.timeStamp;
						    	state.drug.find(x=> x.hash === transaction.hash).pharmacy.status = transaction.status;
						    	state.drug.find(x=> x.hash === transaction.hash).pharmacy.health = "genuine"; 	
						    	state.count++;
							}else{
								if(state.drug.find(x=> x.hash === transaction.hash).health == "compromised"){
									state.drug.find(x=> x.hash === transaction.hash).health = "compromised";
									state.count++;
					    			throw "This drug has been marked compromised as it has skipped verification. Please contact the manufacturer now."
					    		}else{
					    			state.drug.find(x=> x.hash === transaction.hash).health = "compromised";
					    			state.count++;
					    			throw "This drug may have been compromised as it has skipped verification. Please contact the manufacturer now."
					    		}
							}
					    		    	
					    }

					    if(transaction.transactionType === "customerVerify"){
							if(state.drug.find(x=> x.hash === transaction.hash).manufacturer.manufacturerHash != "none" && state.drug.find(x=> x.hash === transaction.hash).shipping.shippingHash != "none" && state.drug.find(x=> x.hash === transaction.hash).warehouse.warehouseHash != null&& state.drug.find(x=> x.hash === transaction.hash).pharmacy.pharmacyHash != null&& state.drug.find(x=> x.hash === transaction.hash).pharmacy.pharmacySellerHash != "none" && state.drug.find(x=> x.hash === transaction.hash).health != "compromised"){
								
								state.count++;
								throw state.drug.find(x=> x.hash === transaction.hash);		    
						    	
							}else{
								if(state.drug.find(x=> x.hash == transaction.hash).health == "compromised"){
									state.drug.find(x=> x.hash == transaction.hash).health = "compromised";
									state.count++;
					    			throw "This drug has been marked compromised as it has skipped verification. Please contact the manufacturer now."
					    		}else{
					    			state.drug.find(x=> x.hash == transaction.hash).health = "compromised";
					    			console.log(state.drug.find(x=> x.hash == transaction.hash).health);
					    			state.count++;
					    			
					    		}
							}
					    		    	
					    }
		    			}else{
		    			state.count++;
		    	 		throw "Warning:This drug does not exist in the blockchain. There is a high possibility that this is a counterfeit product"
		    	}
			   }
		    


}

app.use(transactionHandler)

app.start().then(appInfo => console.log(appInfo.GCI)	)
